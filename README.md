# README

This application shows Single Transaction cost and Multi Sig Transaction cost in USd for differenct currencies.

We add currency chains in seed.rb.
Currency symbol acts acts as STI column, allowing us to have seperate model/class for each currency.

![image](https://i.imgur.com/m2fr2De.png)


* System dependencies
    whenever gem is used.  To update whenever crontab, Use following:
    ```
        whenever --update-crontab
    ```


* Database initialization
    ```
    rails db:seed
    ```

