# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Crypto.create([
                { name: 'Bitcoin', symbol: 'BTC', factor: 2 },
                { name: 'Ethereum', symbol: 'ETH', factor: 20 },
                { name: 'Binance Smart Chain', symbol: 'BNB', factor: 20 },
                { name: 'Bitcoin SV', symbol: 'BSV' }
              ])
