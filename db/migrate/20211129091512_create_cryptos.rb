# frozen_string_literal: true

class CreateCryptos < ActiveRecord::Migration[6.1]
  def change
    create_table :cryptos do |t|
      t.string :name
      t.string :symbol
      t.integer :factor
      t.decimal :single_transaction_cost
      t.bigint :multi_sig_transaction_cost
      t.timestamps
    end
  end
end
