# frozen_string_literal: true

namespace :crypto do
  desc 'set fee for different chains'
  task set_transaction_fee: :environment do
    # Doing it with rake task gives us 1 point of start for all chains.
    # which allows us to make API calls only once

    not_found = []
    response = HTTParty.get('https://api.coincap.io/v2/assets')
    parsed_data = response.parsed_response['data']
    return false if parsed_data.nil?

    currency_by_symbols = parsed_data.group_by { |h| h['symbol'] }

    Crypto.all.find_each do |crypto|
      if currency_by_symbols[crypto.symbol] && currency_by_symbols[crypto.symbol][0]
        crypto.usd_price(currency_by_symbols[crypto.symbol][0]['priceUsd'])
      else
        not_found.push(crypto.symbol)
      end
    end

    puts("No Data Found for: #{not_found}")
  end
end
