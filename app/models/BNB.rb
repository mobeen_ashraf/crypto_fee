# frozen_string_literal: true

class BNB < GasBased
  GAS_COST_URL = 'https://api.bscscan.com/api?module=gastracker&action=gasoracle&apikey=YourApiKeyToken'

  def gas_cost
    response = HTTParty.get(GAS_COST_URL)
    payload = response.parsed_response['result']
    payload['FastGasPrice'].to_f
  end
end
