# frozen_string_literal: true

class ETH < GasBased
  GAS_COST_URL = 'https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey'

  def gas_cost
    response = HTTParty.get(GAS_COST_URL)
    payload = response.parsed_response['result']
    payload['FastGasPrice'].to_f
  end
end
