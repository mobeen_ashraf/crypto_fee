# frozen_string_literal: true

class Crypto < ApplicationRecord
  self.inheritance_column = 'symbol'

  def calculate_multi_sig(single_trx_cost)
    factor ? single_trx_cost * factor : nil
  end

  def usd_price(price_usd)
    single_trx_cost = get_single_transaction_cost(price_usd)
    update(
      single_transaction_cost: single_trx_cost,
      multi_sig_transaction_cost: calculate_multi_sig(single_trx_cost)
    )
  end
end
