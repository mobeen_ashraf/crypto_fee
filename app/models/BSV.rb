# frozen_string_literal: true

class BSV < SatoshiBased
  BYTE_COST_URL = 'https://mapi.taal.com/mapi/feeQuote'

  def satoshi_per_byte
    fee = payload_fee
    mining_fee = fee['miningFee']
    relay_fee = fee['relayFee']
    mining = mining_fee['satoshis'] / mining_fee['bytes'].to_f # 0.5 fixed
    relay = relay_fee['satoshis'] / relay_fee['bytes'].to_f # 0.25 fixed
    mining + relay # 0.75
  end

  def payload_fee
    payload = HTTParty.get(BYTE_COST_URL).parsed_response['payload']
    payload = JSON.parse(payload)
    payload['fees'][0]
  end
end
