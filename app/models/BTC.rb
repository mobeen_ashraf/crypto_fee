# frozen_string_literal: true

class BTC < SatoshiBased
  BYTE_COST_URL = 'https://api.blockchain.info/mempool/fees'

  def satoshi_per_byte
    response = HTTParty.get(BYTE_COST_URL)
    satoshi_per_byte_cost = response.parsed_response['priority']
    satoshi_per_byte_cost.to_f
  end
end
