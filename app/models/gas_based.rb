# frozen_string_literal: true

class GasBased < Crypto
  def get_single_transaction_cost(price_usd)
    21_000 * gas_cost * price_usd.to_f * (10**-9)
  end
end
