# frozen_string_literal: true

class SatoshiBased < Crypto
  def get_single_transaction_cost(price_usd)
    satoshi_per_byte * 192 * price_usd.to_f * (10**-8)
  end
end
