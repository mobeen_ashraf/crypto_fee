# frozen_string_literal: true

json.extract! crypto, :id, :name, :single_transaction_cost, :multi_sig_transaction_cost, :created_at, :updated_at
json.url crypto_url(crypto, format: :json)
