# frozen_string_literal: true

json.array! @cryptos, partial: 'cryptos/crypto', as: :crypto
