# frozen_string_literal: true

class CryptosController < ApplicationController
  # GET /cryptos or /cryptos.json
  def index
    @cryptos = Crypto.all
  end
end
